# eSpark Learning Programming Challenge

### Problem

Teachers across the country have been using eSpark in their classrooms through our free teacher trial program. These represent both a huge opportunity for us to help new students learn reading and math more effectively and for us to convert those districts from trials to customers, allowing us to reach even more students.

The most common way teachers arrive at eSpark is from a referral from someone they know. This is awesome -- it shows the recipient (and us!) that personalized learning is making such a difference that someone wants to share it.

But what would happen if those referred teachers weren't converting? 😨

Sounds like it's time for a special "referred" signup flow!


### Solution

In your programming challenge, you'll need to create a minimal user experience for the new referral flow.

This experience should:

* Confirm/collect information about the teacher (email, name, school, grade(s) they teach)
* Collect useful data for us to analyze
* Include three variations (at whatever level) to explore different hypotheses about parts of the signup process to encourage referred teachers to finish
* We are looking first and foremost for creativity and analytical thinking! We want to see how you think.

That means:

* We won't evaluate you on what framework you use or whether you use scaffolding to build the site
* It's up to you how you collect or store the data (no database needed)
* Tests are optional
* We're more interested in experimental design than polish or shine

### How to submit

It's up to you, but here’s our order of preference: 
1. A URL to a hosted git repo 
3. A zip of a git repo
5. Codepen or similar service
