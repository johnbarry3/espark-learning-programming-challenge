# eSpark Learning Programming Challenge

### Problem
```
* See README_problem-description.md file (in this directory) for the problem description
* See https://docs.google.com/document/d/1iqdJkqxTv4ylrelRu26kRpmmz1Irmvc_ArmlUGQduZQ/edit# 
  for my writeup (it's public to anyone with the link)
```
### Overview
```
A note on the content: all of the text copy is placeholder and intended only to convey the
function / intent internally to the team at eSpark.

This project is:
* a Vue single-page app, created with Vue CLI
* using the vue router plugin for (minimal) routing
* using the vue-bootstrap plugin, which provides vue components that implement bootstrap
  components
* A TOTAL HACK. Really, this is all POC code. I broke lots of rules I would never break
  on a real project around encapsulation, accessibility, CSS overrides, etc.

The app itself is really just a set of static files, all packaged together with some webpack
magic that comes with the Vue CLI.

When it's run locally, the project is served on a super simple node server, which is also
set up by Vue CLI.
```
### Live Link
```
http://espark-learning-p-c.s3-website-us-east-1.amazonaws.com

```
### Project setup
```
If you don't already have it, download and install node and npm:

https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

Then, cd into this directory and run:

npm install
```

### Compiles and hot-reloads core app server for development
```
This command will serve the app on your localhost at port localhost:8080

npm run serve
```

### Build app for production
```
npm run build
```

### Deploy app to production (only works if you're John Barry :)
```
npm run deploy
```
